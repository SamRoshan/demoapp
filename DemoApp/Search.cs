﻿using CommandDotNet.Attributes;
using ConsoleTables;
using DemoApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoApp
{
    public class Search
    {

        /* 
        List<Job> SearchJobs(List<Job> vacancies)

        {
            List<Job> searchResult = new List<Job>();

            Console.Clear();
            Console.Write("Search: ");
            string searchPhrase = Console.ReadLine().ToLower();

            foreach (Job job in vacancies)
            {
                if (job.Titel.Contains(searchPhrase) || job.Employer.Contains(searchPhrase))
                    searchResult.Add(job);
            }

            return searchResult;
        }
        */


        [ApplicationMetadata(Description = "Lists available jobs:")]
        public void Vacancies()
        {
            ConsoleTable
                .From(Jobs.GetJobs())
                .Write();
        }

        [ApplicationMetadata(Description = "Lists train schedule:")]
        public void TimeTable(string destination = "")
        {
            var train = Trains.GetTrains()
                .Where(timetable => timetable.Destination.Contains(destination));

            ConsoleTable
               .From(train)
               .Write();
        }
    }

}
