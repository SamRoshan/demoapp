﻿using CommandDotNet;
using System;
using System.Collections.Generic;

namespace DemoApp
{
    class Program
    {
        static int Main(string[] args)
        {
            AppRunner<Search> appRunner = new AppRunner<Search>();
            return appRunner.Run(args);

        }
    }
}
