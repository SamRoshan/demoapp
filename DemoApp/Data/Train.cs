﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoApp.Data
{
    public class Train
    {
        public string TrainNumber { get; set; }
        public string Destination { get; set; }
        public DateTime DepTime { get; set; }
        public DateTime ArrTime { get; set; }
    }

    public static class Trains
    {
        public static IEnumerable<Train> GetTrains()
        {
            yield return new Train
            {
                TrainNumber = "SJ300",
                Destination = "Stockholm",
                DepTime = new DateTime(2019, 07, 28, 22, 35, 5)
            };

            yield return new Train
            {
                TrainNumber = "SJ100",
                Destination = "Malmo",
                DepTime = new DateTime(2019, 08, 29, 12, 15, 0)
            };
        }
    }
}
