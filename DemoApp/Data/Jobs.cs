﻿using System;
using System.Collections.Generic;

namespace DemoApp
{
    public class Job
    {
        public int JobId { get; set; }
        public DateTime PublishDate { get; set; }
        public string Titel { get; set; }
        public string Employer { get; set; }

    }

    public static class Jobs
    {
        public static IEnumerable<Job> GetJobs()
        {
            yield return new Job
            {
                JobId = 100,
                PublishDate = new DateTime(2019, 3, 1, 7, 0, 0),
                Titel = "Software developer",
            };
           
            yield return new Job
            {
                JobId = 101,
                PublishDate = new DateTime(2019, 5, 3, 9, 0, 0),
                Titel = "Scrum master"
            };
        }
    }
}
